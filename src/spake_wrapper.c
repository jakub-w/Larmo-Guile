#include <string.h>
#include <libguile.h>
#include <sodium.h>

/* #define sodium_malloc(x) malloc(x) */
/* #define sodium_free(x) free(x) */

#include "dependencies/spake2-ee/src/crypto_spake.h"


SCM_VARIABLE_INIT(spake_public_data_bytes, "spake-public-data-bytes",
                  scm_from_int(crypto_spake_PUBLICDATABYTES));
SCM_VARIABLE_INIT(spake_response1_bytes, "spake-response1-bytes",
                  scm_from_int(crypto_spake_RESPONSE1BYTES));
SCM_VARIABLE_INIT(spake_response2_bytes, "spake-response2-bytes",
                  scm_from_int(crypto_spake_RESPONSE2BYTES));
SCM_VARIABLE_INIT(spake_response3_bytes, "spake-response3-bytes",
                  scm_from_int(crypto_spake_RESPONSE3BYTES));
SCM_VARIABLE_INIT(spake_shared_key_bytes, "spake-shared-key-bytes",
                  scm_from_int(crypto_spake_SHAREDKEYBYTES));
SCM_VARIABLE_INIT(spake_stored_bytes, "spake-stored-bytes",
                  scm_from_int(crypto_spake_STOREDBYTES));

SCM spake_client_state_type;
SCM spake_server_state_type;
SCM spake_shared_keys_type;

SCM_DEFINE(make_client_state, "make-spake-client-state", 0, 0, 0, (),)
#define FUNC_NAME s_make_client_state
{
  crypto_spake_client_state* state =
      scm_gc_malloc_pointerless(sizeof(crypto_spake_client_state),
                                "spake-client-state");
  return scm_make_foreign_object_1(spake_client_state_type, state);
}
#undef FUNC_NAME

void finalize_client_state(SCM o) {
  crypto_spake_client_state* state = scm_foreign_object_ref(o, 0);
  sodium_memzero(state, sizeof(crypto_spake_client_state));
}

SCM_DEFINE(client_state_p, "spake-client-state?", 1, 0, 0, (SCM o),)
#define FUNC_NAME s_client_state_p
{
  return scm_from_bool(SCM_IS_A_P(o, spake_client_state_type));
}
#undef FUNC_NAME

SCM_DEFINE(client_state_to_ptr, "spake-client-state->pointer", 1, 0, 0,
           (SCM o),)
#define FUNC_NAME s_client_state_to_ptr
{
  scm_assert_foreign_object_type(spake_client_state_type, o);

  return scm_from_pointer(scm_foreign_object_ref(o, 0), NULL);
}
#undef FUNC_NAME

SCM_DEFINE(make_server_state, "make-spake-server-state", 0, 0, 0, (),)
#define FUNC_NAME s_make_server_state
{
  crypto_spake_client_state* state =
      scm_gc_malloc_pointerless(sizeof(crypto_spake_server_state),
                                "spake-server-state");
  return scm_make_foreign_object_1(spake_server_state_type, state);
}
#undef FUNC_NAME

void finalize_server_state(SCM o) {
  crypto_spake_server_state* state = scm_foreign_object_ref(o, 0);
  sodium_memzero(state, sizeof(crypto_spake_server_state));
}

SCM_DEFINE(server_state_p, "spake-server-state?", 1, 0, 0, (SCM o),)
#define FUNC_NAME s_server_state_p
{
  return scm_from_bool(SCM_IS_A_P(o, spake_server_state_type));
}
#undef FUNC_NAME

SCM_DEFINE(server_state_to_ptr, "spake-server-state->pointer", 1, 0, 0,
           (SCM o),)
#define FUNC_NAME s_server_state_to_ptr
{
  scm_assert_foreign_object_type(spake_server_state_type, o);

  return scm_from_pointer(scm_foreign_object_ref(o, 0), NULL);
}
#undef FUNC_NAME

SCM_DEFINE(make_shared_keys, "make-spake-shared-keys", 0, 0, 0, (),)
#define FUNC_NAME s_make_shared_keys
{
  crypto_spake_shared_keys* keys =
      scm_gc_malloc_pointerless(sizeof(crypto_spake_shared_keys),
                                "spake-shared-keys");
  return scm_make_foreign_object_1(spake_shared_keys_type, keys);
}
#undef FUNC_NAME

void finalize_shared_keys(SCM o) {
  crypto_spake_shared_keys* keys = scm_foreign_object_ref(o, 0);
  sodium_memzero(keys, sizeof(crypto_spake_shared_keys));
}

SCM_DEFINE(shared_keys_p, "spake-shared-keys?", 1, 0, 0, (SCM o),)
#define FUNC_NAME s_shared_keys_p
{
  return scm_from_bool(SCM_IS_A_P(o, spake_shared_keys_type));
}
#undef FUNC_NAME

SCM_DEFINE(shared_keys_get_client_sk, "spake-shared-keys-get-client-key",
           1, 0, 0, (SCM o),)
#define FUNC_NAME s_shared_keys_get_client_sk
{
  scm_assert_foreign_object_type(spake_shared_keys_type, o);

  crypto_spake_shared_keys* keys = scm_foreign_object_ref(o, 0);

  SCM bv = scm_c_make_bytevector(crypto_spake_SHAREDKEYBYTES);
  unsigned char* bv_ptr = SCM_BYTEVECTOR_CONTENTS(bv);
  memcpy(bv_ptr, keys->client_sk, crypto_spake_SHAREDKEYBYTES);

  return bv;
}
#undef FUNC_NAME

SCM_DEFINE(shared_keys_get_server_sk, "spake-shared-keys-get-server-key",
           1, 0, 0, (SCM o),)
#define FUNC_NAME s_shared_keys_get_server_sk
{
  scm_assert_foreign_object_type(spake_shared_keys_type, o);

  crypto_spake_shared_keys* keys = scm_foreign_object_ref(o, 0);

  SCM bv = scm_c_make_bytevector(crypto_spake_SHAREDKEYBYTES);
  unsigned char* bv_ptr = SCM_BYTEVECTOR_CONTENTS(bv);
  memcpy(bv_ptr, keys->server_sk, crypto_spake_SHAREDKEYBYTES);

  return bv;
}
#undef FUNC_NAME

SCM_DEFINE(shared_keys_to_ptr, "spake-shared-keys->pointer", 1, 0, 0,
           (SCM o),)
#define FUNC_NAME s_client_state_to_ptr
{
  scm_assert_foreign_object_type(spake_shared_keys_type, o);

  return scm_from_pointer(scm_foreign_object_ref(o, 0), NULL);
}
#undef FUNC_NAME

void init_spake_wrapper() {
#ifndef SCM_MAGIC_SNARFER
#include "spake_wrapper.x"
#endif

  spake_client_state_type = scm_make_foreign_object_type(
      scm_from_utf8_symbol("client-state"),
      scm_list_1(scm_from_utf8_symbol("data")),
      finalize_client_state);

  spake_server_state_type = scm_make_foreign_object_type(
      scm_from_utf8_symbol("server-state"),
      scm_list_1(scm_from_utf8_symbol("data")),
      finalize_server_state);

  spake_shared_keys_type = scm_make_foreign_object_type(
      scm_from_utf8_symbol("spake-shared-keys"),
      scm_list_1(scm_from_utf8_symbol("data")),
      finalize_shared_keys);
}
