#!/usr/bin/sh

DIR="$(dirname "$(realpath "$0")")/deps"
mkdir --parents "$DIR"


build_directory() {
    echo Switching to directory "$1"
    pushd "$1" >/dev/null

    [ -x configure ] || autoreconf -vif || exit $?
    mkdir --parents build && cd build
    [ -f Makefile ] || ../configure --prefix="$DIR" || exit $?
    make -j$(nproc) install || exit $?

    popd
}


build_directory dependencies/guile-rpc/
build_directory dependencies/fibers/
build_directory dependencies/guile-simple-zmq/
