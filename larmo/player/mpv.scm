;; Copyrignt (C) 2021-2022 Jakub Wojciech
;;
;; This file is part of Larmo.
;;
;; Larmo is free software: you can redistribute it and/or modify it under
;; the terms of the GNU Affero General Public License as published by the
;; Free Software Foundation, either version 3 of the License, or (at your
;; option) any later version.
;;
;; Larmo is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
;; FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
;; License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with Larmo. If not, see <https://www.gnu.org/licenses/>.

(library (larmo player mpv)
  (export mpv-player)
  (import (larmo log)
	  (system foreign-library)
	  (system foreign)
	  (srfi srfi-9)
	  (rnrs bytevectors)
	  (ice-9 atomic)
	  (ice-9 binary-ports)
	  (ice-9 exceptions)
	  (ice-9 match)
	  (fibers)
	  (fibers scheduler)))


;; This is required for libmpv to initialize and function correctly.
(setlocale LC_NUMERIC "C")

(define libmpv (load-foreign-library "libmpv"))

;; mpv-error
(define MPV_ERROR_SUCCESS 0)
(define MPV_ERROR_EVENT_QUEUE_FULL -1)
(define MPV_ERROR_NOMEM -2)
(define MPV_ERROR_UNINITIALIZED -3)
(define MPV_ERROR_INVALID_PARAMETER -4)
(define MPV_ERROR_OPTION_NOT_FOUND -5)
(define MPV_ERROR_OPTION_FORMAT -6)
(define MPV_ERROR_OPTION_ERROR -7)
(define MPV_ERROR_PROPERTY_NOT_FOUND -8)
(define MPV_ERROR_PROPERTY_FORMAT -9)
(define MPV_ERROR_PROPERTY_UNAVAILABLE -10)
(define MPV_ERROR_PROPERTY_ERROR -11)
(define MPV_ERROR_COMMAND -12)
(define MPV_ERROR_LOADING_FAILED -13)
(define MPV_ERROR_AO_INIT_FAILED -14)
(define MPV_ERROR_VO_INIT_FAILED -15)
(define MPV_ERROR_NOTHING_TO_PLAY -16)
(define MPV_ERROR_UNKNOWN_FORMAT -17)
(define MPV_ERROR_UNSUPPORTED -18)
(define MPV_ERROR_NOT_IMPLEMENTED -19)
(define MPV_ERROR_GENERIC -20)

;; mpv_event_id
(define MPV_EVENT_NONE 0)
(define MPV_EVENT_SHUTDOWN 1)
(define MPV_EVENT_LOG_MESSAGE 2)
(define MPV_EVENT_GET_PROPERTY_REPLY 3)
(define MPV_EVENT_SET_PROPERTY_REPLY 4)
(define MPV_EVENT_COMMAND_REPLY 5)
(define MPV_EVENT_START_FILE 6)
(define MPV_EVENT_END_FILE 7)
(define MPV_EVENT_FILE_LOADED 8)
(define MPV_EVENT_CLIENT_MESSAGE 16)
(define MPV_EVENT_VIDEO_RECONFIG 17)
(define MPV_EVENT_AUDIO_RECONFIG 18)
(define MPV_EVENT_SEEK 20)
(define MPV_EVENT_PLAYBACK_RESTART 21)
(define MPV_EVENT_PROPERTY_CHANGE 22)
(define MPV_EVENT_QUEUE_OVERFLOW 24)
(define MPV_EVENT_HOOK 25)
;; deprecated
(define MPV_EVENT_TRACKS_CHANGED 9)
(define MPV_EVENT_TRACK_SWITCHED 10)
(define MPV_EVENT_IDLE 11)
(define MPV_EVENT_PAUSE 12)
(define MPV_EVENT_UNPAUSE 13)
(define MPV_EVENT_TICK 14)
(define MPV_EVENT_SCRIPT_INPUT_DISPATCH 15)
(define MPV_EVENT_METADATA_UPDATE 19)
(define MPV_EVENT_CHAPTER_CHANGE 23)
;; end of deprecated

;; mpv_end_file_reason
(define MPV_END_FILE_REASON_EOF 0)
(define MPV_END_FILE_REASON_STOP 2)
(define MPV_END_FILE_REASON_QUIT 3)
(define MPV_END_FILE_REASON_ERROR 4)
(define MPV_END_FILE_REASON_REDIRECT 5)

;; mpv_format
(define MPV_FORMAT_NONE 0)
(define MPV_FORMAT_STRING 1)
(define MPV_FORMAT_OSD_STRING 2)
(define MPV_FORMAT_FLAG 3)
(define MPV_FORMAT_INT64 4)
(define MPV_FORMAT_DOUBLE 5)
(define MPV_FORMAT_NODE 6)
(define MPV_FORMAT_NODE_ARRAY 7)
(define MPV_FORMAT_NODE_MAP 8)
(define MPV_FORMAT_BYTE_ARRAY 9)


(define-record-type <mpv-context>
  (make-mpv-context ptr)
  mpv-context?
  (ptr mpv-context-ptr mpv-context-set-ptr))

(define-syntax-rule (assert-type pred obj type subr)
  (unless (pred obj)
    (throw 'wrong-type-arg subr
	   "Wrong type argument. Expected ~a got: ~a"
	   (list type obj))))

(define mpv-error-string
  (let ((func (pointer->procedure
	       '*
	       (foreign-library-pointer libmpv "mpv_error_string")
	       (list int))))
    (lambda (error-code)
      (pointer->string (func error-code)))))

;; TODO: Provide error code in addition to the message.
;;       A new exception type will have to be defined.
(define* (mpv-error subr error-code #:optional irritants)
  (raise-exception
   (apply make-exception
	  (make-exception-with-origin subr)
	  (make-exception-with-message (mpv-error-string error-code))
	  (if irritants
	      (list (make-exception-with-irritants irritants))
	      '()))))

(define mpv-destroy!
  (let ((destroy (pointer->procedure
	       '*
	       (foreign-library-pointer libmpv "mpv_terminate_destroy")
	       '(*))))
    (lambda (ctx)
      (assert-type mpv-context? ctx 'mpv-context 'mpv-destroy!)
      (destroy (mpv-context-ptr ctx))
      (mpv-context-set-ptr ctx %null-pointer)
      (if #f #f))))

(define mpv-create
  (let ((create (pointer->procedure
		 '*
		 (foreign-library-pointer libmpv "mpv_create")
		 '()))
	(initialize (pointer->procedure
		     int
		     (foreign-library-pointer libmpv "mpv_initialize")
		     '(*))))
    (lambda ()
      "Create an mpv context. mpv-destroy! must be called at termination."
      (let ((ptr (create)))
	(if (null-pointer? ptr)
	    #f
	    (let ((ctx (make-mpv-context ptr))
		  (err (initialize ptr)))
	      (if (= err MPV_ERROR_SUCCESS)
		  ctx
		  (begin (mpv-destroy! ctx)
			 (mpv-error 'mpv-create err)))))))))

(define* (mpv-check-error error-code
			  #:optional (subr #f)
			  #:key irritants)
  (unless (= error-code MPV_ERROR_SUCCESS)
    (if irritants
	(mpv-error subr error-code irritants)
	(mpv-error subr error-code))))

(define mpv-free
  (let ((free (foreign-library-function libmpv
					"mpv_free"
					#:arg-types '(*))))
    (lambda (ptr) (free ptr))))

;; Add handled properties by adding them to type-alist
;; TODO: Some properties don't have values in certain periods of the runtime.
;;       E.g. time-remaining while nothing is playing.
;;       In such cases MPV_ERROR_PROPERTY_UNAVAILABLE is returned.
;;       It probably shouldn't be an error. Maybe?
(define mpv-get-property
  (let ((get-prop! (foreign-library-function
		    libmpv
		    "mpv_get_property"
		    #:return-type int
		    #:arg-types (list '* '* int '*)))
	(free (foreign-library-pointer libmpv "mpv_free"))
	(free-node (foreign-library-pointer libmpv "mpv_free_node_contents"))
	(type-alist '((idle-active . boolean)
		      (log-file . string)
		      (force-seekable . boolean)
		      (time-pos . double)
		      (time-remaining . double)
		      (duration . double)
		      (playtime-remaining . double)
		      (volume . int64))))
    (lambda (ctx name)
      (assert-type mpv-context? ctx 'mpv-context 'mpv-get-property)
      (let ((prop-type (assoc-ref type-alist name)))
	(assert-type
	 (lambda (prop) (and (symbol? prop) prop-type))
	 name "a valid property symbol" 'mpv-get-property)
	(case prop-type
	  ((boolean)
	   (let* ((bv (make-bytevector 4))
		  (ptr (bytevector->pointer bv)))
	     (mpv-check-error
	      (get-prop! (mpv-context-ptr ctx)
			 (string->pointer (symbol->string name))
			 MPV_FORMAT_FLAG
			 ptr)
	      'mpv-get-property)
	     (= 1 (bytevector-s32-native-ref bv 0))))
	  ((int64)
	   (let* ((bv (make-bytevector 8))
		  (ptr (bytevector->pointer bv)))
	     (mpv-check-error
	      (get-prop! (mpv-context-ptr ctx)
			 (string->pointer (symbol->string name))
			 MPV_FORMAT_INT64
			 ptr)
	      'mpv-get-property)
	     (bytevector-s64-native-ref bv 0)))
	  ((double)
	   (let* ((bv (make-bytevector 8))
		  (ptr (bytevector->pointer bv)))
	     (mpv-check-error
	      (get-prop! (mpv-context-ptr ctx)
			 (string->pointer (symbol->string name))
			 MPV_FORMAT_DOUBLE
			 ptr)
	      'mpv-get-property)
	     (bytevector-ieee-double-native-ref bv 0)))
	  ((string)
	   (let* ((bv (make-bytevector 8))
		  (ptr (bytevector->pointer bv)))
	     (mpv-check-error
	      (get-prop! (mpv-context-ptr ctx)
			 (string->pointer (symbol->string name))
			 MPV_FORMAT_STRING
			 ptr)
	      'mpv-get-property)
	     (pointer->string
	      (make-pointer (bytevector-u64-native-ref bv 0)
			    free)))))))))

(mpv-get-property context 'log-file)




(define mpv-set-property!
  (let ((set-prop (pointer->procedure
		   int
		   (foreign-library-pointer libmpv "mpv_set_property")
		   `(* * ,int *)))
	(set-str (pointer->procedure
		   int
		   (foreign-library-pointer libmpv "mpv_set_property_string")
		   '(* * *))))
    (lambda (ctx name value)
      (assert-type mpv-context? ctx 'mpv-context 'mpv-set-property!)
      (assert-type symbol? name "a valid property symbol" 'mpv-set-property!)
      (mpv-check-error
       (cond
	((string? value)
	 (set-str (mpv-context-ptr ctx)
		  (string->pointer (symbol->string name))
		  (string->pointer value)))
	((boolean? value)
	 (let ((bv (make-bytevector 4)))
	   (bytevector-s32-native-set! bv 0 (if value 1 0))
	   (set-prop (mpv-context-ptr ctx)
		     (string->pointer (symbol->string name))
		     MPV_FORMAT_FLAG
		     (bytevector->pointer bv))))
	((exact-integer? value)
	 (let ((bv (make-bytevector 8)))
	   (bytevector-s64-native-set! bv 0 value)
	   (set-prop (mpv-context-ptr ctx)
		     (string->pointer (symbol->string name))
		     MPV_FORMAT_INT64
		     (bytevector->pointer bv))))
	((real? value)
	 (let ((bv (make-bytevector 8)))
	   (bytevector-ieee-double-native-set! bv 0 value)
	   (set-prop (mpv-context-ptr ctx)
		     (string->pointer (symbol->string name))
		     MPV_FORMAT_DOUBLE
		     (bytevector->pointer bv))))
	(else (raise-exception
	       (make-exception
		(make-exception-with-message "Value type not handled")
		(make-exception-with-irritants value)
		(make-exception-with-origin 'mpv-set-property)
		(make-non-continuable-error)))))
       'mpv-set-property))))

(define mpv-set-log-level!
  (let ((req-log (pointer->procedure
		  int
		  (foreign-library-pointer libmpv "mpv_request_log_messages")
		  `(* *)))
	(log-levels '(fatal error warn info v debug trace)))
    (lambda (ctx log-level)
      (assert-type mpv-context? ctx 'mpv-context 'mpv-set-log-level!)
      (when (and log-level (not (member log-level log-levels)))
	(throw 'wrong-type-arg 'mpv-set-property
	       "Wrong type argument. Expected a symbol among ~a, got: ~a"
	       (list log-levels log-level)))
      (mpv-check-error
       (if log-level
	   (req-log (mpv-context-ptr ctx)
		    (string->pointer (symbol->string log-level)))
	   (req-log (mpv-context-ptr ctx)
		    (string->pointer "no")))
       'mpv-set-log-level))))

(define mpv-command
  (let ((command (foreign-library-function libmpv
					   "mpv_command_string"
					   #:return-type int
					   #:arg-types '(* *))))
    (lambda (ctx cmd)
      (assert-type mpv-context? ctx "mpv-context" 'mpv-command)
      (assert-type string? cmd "string" 'mpv-command)
      (mpv-check-error
       (command (mpv-context-ptr ctx) (string->pointer cmd))
       'mpv-command
       #:irritants cmd))))

(define (mpv-play ctx port)
  (define (port->file-port)
    (if (file-port? port)
	port
	(match (pipe)
	  ((input . output)
	   (spawn-fiber (lambda ()
			  (let loop ((byte (get-u8 port)))
			    (if (eof-object? byte)
				(close output)
				(begin
				  (put-u8 output byte)
				  (loop (get-u8 port))))))
			#:parallel? #t)
	   input))))

  (assert-type mpv-context? ctx "mpv-context" 'mpv-play)
  (assert-type port? port "port" 'mpv-play)

  (let ((fd (port->fdes (port->file-port))))
    (mpv-command
     ctx
     (string-append/shared "loadfile " "fdclose://" (number->string fd)))))

(define (mpv-stop ctx)
  (assert-type mpv-context? ctx "mpv-context" 'mpv-stop)
  (mpv-command ctx "stop"))

;; (mpv-play context (open-input-file "/tmp/test.mp3" #:binary #t))
;; (mpv-play context (open-input-file "/tmp/test1.lol" #:binary #t))
;; (mpv-stop context)
;; (gc)

;; FIXME: This requires to be terminated and freed.
;;        Probably instead of creating it in the global environment, make a
;;        (with-mpv ...) macro that will create the context and remove it
;;        on exit.
(define context (mpv-create))

;; (mpv-set-property! context 'log-file "mpv.log")
;; (mpv-set-property! context 'video #f)
;; (mpv-set-property! context 'force-seekable #t)
;; (mpv-set-log-level! context 'debug)
;; (mpv-set-log-level! context 'trace)

(define mpv-observe-property!
  (let ((observe (foreign-library-function libmpv
					   "mpv_observe_property"
					   #:return-type int
					   #:arg-types `(* ,uint64 * ,int))))
    (lambda (ctx reply-userdata name fmt)
      (assert-type mpv-context? ctx 'mpv-context 'mpv-observe-property!)
      (assert-type integer? reply-userdata 'integer 'mpv-observe-property!)
      (assert-type string? name 'string 'mpv-observe-property!)
      (assert-type integer? fmt 'integer 'mpv-observe-property!)
      (mpv-check-error
       (observe (mpv-context-ptr ctx)
		reply-userdata
		(string->pointer name)
		fmt)))))

(define mpv-wakeup
  (let ((wakeup (foreign-library-function libmpv
					  "mpv_wakeup"
					  #:arg-types '(*))))
    (lambda (ctx)
      (wakeup (mpv-context-ptr ctx)))))



(define event-loop-running #f)

;; TODO: Start mpv event loop.
(define (mpv-run-event-loop ctx)
  (define mpv-wait-event
    (let ((wait-event (foreign-library-function
		       libmpv
		       "mpv_wait_event"
		       #:return-type '*
		       #:arg-types (list '* double))))
      (lambda (timeout)
	(parse-c-struct
	 (wait-event (mpv-context-ptr ctx) timeout)
	 (list int int uint64 '*)))))

  (define (handle-eof data)
    (match data
      ((reason error playlist-entry-id playlist-insert-id
	       playlist-insert-num-entries)
       ;; TODO: Check the error
       ;; TODO: Do something
       (case reason
	 ((MPV_END_FILE_REASON_EOF) #f)  ; End of file reached
	 ((MPV_END_FILE_REASON_STOP) #f) ; Stopped explicitly
	 ((MPV_END_FILE_REASON_QUIT) #f) ; Quit command or player shutdown
	 ((MPV_END_FILE_REASON_ERROR) #f)
	 ((MPV_END_FILE_REASON_REDIRECT) #f) ; Playlist was replaced
	 ))))

  (define (handle-property-change data)
    (match data
      (("pause" _ int-ptr)
       ;; TODO: Handle the change
       (let ((paused? (= 1 (bytevector-s32-native-ref
			    (pointer->bytevector int-ptr 4) 0))))
	 (cond ((paused?) 'paused)
	       ((not (mpv-get-property context 'idle-active))
		;; When paused, loading a new file will start the playback and
		;; the "paused" property will change to #f and this will run.
		'playing)
	       (else 'stopped))))
      (_ (if #f #f))))

  (assert-type mpv-context? ctx 'mpv-context 'mpv-event-loop)
  (mpv-observe-property! ctx 0 "pause" MPV_FORMAT_FLAG)
  (spawn-fiber
   (lambda ()
     (log-info "MPV event loop started")
     (while event-loop-running
       ;; NOTE: This can be interrupted at any moment with mpv_wakeup().
       (match (mpv-wait-event 0.5)
	 ((event-id error reply-userdata data-ptr)
	  (cond
	   ((not (= error MPV_ERROR_SUCCESS))
	    ;; (log-error "mpv event '~a': ~a"
	    ;; 		(mpv-event-name event-id)
	    ;; 		(mpv-error-string error))
	    )
	   ((= event-id MPV_EVENT_NONE)) ; Timeout reached
	   ((= event-id MPV_EVENT_SHUTDOWN)
	    ;; TODO: Should destroy the context.
	    )
	   ((= event-id MPV_EVENT_END_FILE)
	    (handle-eof
	     (parse-c-struct data-ptr (list int int int64 int64 int))))
	   ((= event-id MPV_EVENT_FILE_LOADED)
	    ;; TODO: If paused, state doesn't change. If not paused, the file
	    ;;       starts playing.
	    )
	   ((= event-id MPV_EVENT_SEEK)) ; Seek started
	   ((= event-id MPV_EVENT_PLAYBACK_RESTART)) ; Resume after seeking
	   ((= event-id MPV_EVENT_PROPERTY_CHANGE) ; Observed property changed
	    (handle-property-change
	     (parse-c-struct data-ptr (list '* int '*)))))))
       (yield-current-task))
     (log-info "MPV event loop stopped"))
   #:parallel? #t))

(define (mpv-stop-event-loop ctx)
  (set! event-loop-running #f)
  (mpv-wakeup ctx))

(run-fibers
 (lambda () (mpv-run-event-loop context))
 #:drain? #t)
(mpv-stop-event-loop context)
