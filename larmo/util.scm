;; Copyrignt (C) 2021-2022 Jakub Wojciech
;;
;; This file is part of Larmo.
;;
;; Larmo is free software: you can redistribute it and/or modify it under
;; the terms of the GNU Affero General Public License as published by the
;; Free Software Foundation, either version 3 of the License, or (at your
;; option) any later version.
;;
;; Larmo is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
;; FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
;; License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with Larmo. If not, see <https://www.gnu.org/licenses/>.

(library (larmo util)
  (export assert-type
	  bytevector-empty?
	  bytevector->hex
	  bytevector-compare
	  pks)
  (import (guile)
	  (ice-9 control)
	  (ice-9 threads)
	  (rnrs bytevectors)))

(define (assert-type pred obj type subr)
  (unless (pred obj)
    (scm-error 'wrong-type-arg subr
	       "Wrong type argument. Expected ~a, got: ~a" (list type obj)
	       (list obj))))

(define (bytevector-empty? bv)
  (let/ec return
    (array-for-each
     (lambda (val) (unless (zero? val) (return #f)))
     bv)
    #t))

(define (bytevector->hex bv)
  (apply string-append (map (lambda (n) (number->string n 16))
			    (bytevector->u8-list bv))))

(define (bytevector-compare bv1 bv1-start bv2 bv2-start count)
  (let loop ((i 0))
    (cond ((= i count) #t)
	  ((= (bytevector-u8-ref bv1 (+ bv1-start i))
	      (bytevector-u8-ref bv2 (+ bv2-start i)))
	   (loop (+ i 1)))
	  (else #f))))

(define (pks . args)
  (monitor (apply pk args)))
