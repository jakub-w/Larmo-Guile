(library (larmo crypto encrypted-port spake)
  (export spake-handshake-client
	  spake-handshake-server)
  (import (guile)
	  (ice-9 control)
	  (rnrs base)
	  (rnrs bytevectors)
	  (rnrs io ports)
	  (srfi srfi-2)
	  (larmo crypto core)
	  (larmo crypto spake)
	  (larmo crypto encrypted-port)
	  (larmo util)))

(define handshake-magic-bytes
  (string->utf8 "SPAKE2-EE-hs"))
(define handshake-magic-bytes-length
  (bytevector-length handshake-magic-bytes))

(define (get-magic-data-compare! port buffer)
  (and (not (eof-object?
	     (get-bytevector-n! port buffer 0 handshake-magic-bytes-length)))
       (bytevector-compare buffer 0 handshake-magic-bytes 0
			   handshake-magic-bytes-length)))

(define (get-id port)
  (and-let* ((id-len (get-u8 port))
	     (_ (not (eof-object? id-len)))
	     (id (make-bytevector id-len))
	     (_ (get-bytevector-n! port id 0 id-len)))
    id))

(define (spake-handshake-client password client-id)
  "Max size of CLIENT-ID is 256 bytes."
  (assert (string? password))
  (assert (or (string? client-id) (bytevector? client-id)))

  (when (string? client-id)
    (set! client-id (string->utf8 client-id)))
  (when (> (bytevector-length client-id) 256)
    (error 'client-id-too-long))

  (lambda (port)
    (define data (make-bytevector spake-buffer-size))

    (let/ec return
      (unless (get-magic-data-compare! port data) (return #f))

      (define server-id (get-id port))
      (unless server-id (return #f))

      (when (eof-object?
	     (get-bytevector-n! port data 0 spake-public-data-bytes))
	(return #f))

      (define client-state (spake-step-1! data password))
      (unless (spake-client-state? client-state) (return #f))

      (put-bytevector port handshake-magic-bytes)
      (put-u8 port (bytevector-length client-id))
      (put-bytevector port client-id)
      (put-bytevector port data 0 spake-response1-bytes)
      (force-output port)

      (when (eof-object?
	     (get-bytevector-n! port data 0 spake-response2-bytes))
	(return #f))

      (define shared-keys
	(spake-step-3! client-state data client-id server-id))
      (unless (sodium-encryption-keys? shared-keys) (return #f))

      (put-bytevector port data 0 spake-response3-bytes)

      (define encryption-state
	(make-sodium-encryption-state! data shared-keys))
      (put-bytevector port data 0 sodium-secretstream-header-bytes)
      (force-output port)

      (when (eof-object? (get-bytevector-n!
			  port data 0 sodium-secretstream-header-bytes))
	(return #f))
      (encryption-state data))))

(define (spake-handshake-server shadowed-password server-id)
  "Max size of SERVER-ID is 256 bytes."
  (assert (bytevector? shadowed-password))
  (assert (or (string? server-id) (bytevector? server-id)))

  (when (string? server-id)
    (set! server-id (string->utf8 server-id)))
  (when (> (bytevector-length server-id) 256)
    (error 'server-id-too-long))

  (lambda (port)
    (define data (make-bytevector spake-buffer-size))

    (let/ec return
      (define server-state (spake-step-0! data shadowed-password))
      (unless server-state
	;; FIXME: Proper error handling.
	(error 'bad-shadowed-password)
	(return #f))

      (put-bytevector port handshake-magic-bytes)
      (put-u8 port (bytevector-length server-id))
      (put-bytevector port server-id)
      (put-bytevector port data 0 spake-public-data-bytes)

      (force-output port)

      (unless (get-magic-data-compare! port data) (return #f))
      (define client-id (get-id port))
      (unless client-id (return #f))
      (when (eof-object?
	     (get-bytevector-n! port data 0 spake-response1-bytes))
	(return #f))

      (unless (spake-step-2! server-state data shadowed-password
			     server-id client-id)
	(return #f))


      (put-bytevector port data 0 spake-response2-bytes)
      (force-output port)

      (when (eof-object?
	     (get-bytevector-n! port data 0 spake-response3-bytes))
	(return #f))

      (define shared-keys (spake-step-4! server-state data))
      (unless (sodium-encryption-keys? shared-keys) (return #f))

      (when (eof-object? (get-bytevector-n!
			  port data 0 sodium-secretstream-header-bytes))
	(return #f))

      (define encryption-state
	(make-sodium-encryption-state-complete! data data shared-keys))
      (unless (sodium-encryption-state? encryption-state) (return #f))

      (put-bytevector port data 0 sodium-secretstream-header-bytes)
      (force-output port)

      encryption-state)))
