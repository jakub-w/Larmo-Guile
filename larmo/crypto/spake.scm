;; Copyrignt (C) 2022 Jakub Wojciech
;;
;; This file is part of Larmo.
;;
;; Larmo is free software: you can redistribute it and/or modify it under
;; the terms of the GNU Affero General Public License as published by the
;; Free Software Foundation, either version 3 of the License, or (at your
;; option) any later version.
;;
;; Larmo is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
;; FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
;; License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with Larmo. If not, see <https://www.gnu.org/licenses/>.

(library (larmo crypto spake)
  (export spake-buffer-size
	  sodium-opslimit-min
	  sodium-opslimit-max
	  sodium-opslimit-interactive
	  sodium-opslimit-moderate
	  sodium-opslimit-sensitive
	  sodium-memlimit-min
	  sodium-memlimit-max
	  sodium-memlimit-interactive
	  sodium-memlimit-moderate
	  sodium-memlimit-sensitive
	  spake-public-data-bytes
	  spake-response1-bytes
	  spake-response2-bytes
	  spake-response3-bytes
	  spake-server-state?
	  spake-client-state?
	  spake-shadow-password
	  spake-step-0!
	  spake-step-1!
	  spake-step-2!
	  spake-step-3!
	  spake-step-4!)
  (import (guile)
	  (rnrs base)
	  (rnrs bytevectors)
	  (system foreign)
	  (system foreign-library)
	  (larmo crypto core)))

(eval-when (load compile)
  (define libsodium (load-foreign-library "libsodium"))
  (define libspake (load-foreign-library "liblarmo_spake"))

  ((foreign-library-function libspake "init_spake_wrapper")))

(define spake-buffer-size (max spake-public-data-bytes
			       spake-response1-bytes
			       spake-response2-bytes
			       spake-response3-bytes
			       spake-shared-key-bytes))

(define &spake-server-store
  (foreign-library-function libspake "crypto_spake_server_store"
			    #:return-type int
			    #:arg-types `(* * ,size_t ,size_t ,size_t)))

(define &spake-step-0
  (foreign-library-function libspake "crypto_spake_step0"
			    #:return-type int
			    #:arg-types `(* * *)))

(define &spake-step-1
  (foreign-library-function libspake "crypto_spake_step1"
			    #:return-type int
			    #:arg-types `(* * * * ,size_t)))

(define &spake-step-2
  (foreign-library-function libspake "crypto_spake_step2"
			    #:return-type int
			    #:arg-types `(* * * ,size_t * ,size_t * *)))

(define &spake-step-3
  (foreign-library-function libspake "crypto_spake_step3"
			    #:return-type int
			    #:arg-types `(* * * * ,size_t * ,size_t *)))

(define &spake-step-4
  (foreign-library-function libspake "crypto_spake_step4"
			    #:return-type int
			    #:arg-types `(* * *)))

(define (spake-shared-keys->sodium-encryption-keys-client keys)
  (assert (spake-shared-keys? keys))

  ((@@ (larmo crypto encrypted-port) &make-sodium-encryption-keys)
   (spake-shared-keys-get-client-key keys)
   (spake-shared-keys-get-server-key keys)))

(define (spake-shared-keys->sodium-encryption-keys-server keys)
  (assert (spake-shared-keys? keys))

  ((@@ (larmo crypto encrypted-port) &make-sodium-encryption-keys)
   (spake-shared-keys-get-server-key keys)
   (spake-shared-keys-get-client-key keys)))


(define* (spake-shadow-password password
				#:key
				(ops-limit sodium-opslimit-interactive)
				(mem-limit sodium-memlimit-interactive))
  (assert (string? password))
  (assert (integer? ops-limit))
  (assert (<= sodium-opslimit-min ops-limit sodium-opslimit-max))
  (assert (integer? mem-limit))
  (assert (<= sodium-memlimit-min mem-limit sodium-memlimit-max))

  (when (string-null? password)
    (error 'spake-shadow-password "password empty"))

  (let ((ret (make-bytevector spake-stored-bytes))
	(pass-bv (string->utf8 password)))
    (and (= 0 (&spake-server-store
	       (bytevector->pointer ret)
	       (bytevector->pointer pass-bv)
	       (bytevector-length pass-bv)
	       ops-limit
	       mem-limit))
	 ret)))

(define (spake-step-0! data shadowed-password)
  (assert (bytevector? data))
  (assert (>= (bytevector-length data) spake-public-data-bytes))
  (assert (bytevector? shadowed-password))
  (assert (= (bytevector-length shadowed-password) spake-stored-bytes))

  (let ((state (make-spake-server-state)))
    (and (= 0 (&spake-step-0 (spake-server-state->pointer state)
			     (bytevector->pointer data)
			     (bytevector->pointer shadowed-password)))
	 state)))

(define (spake-step-1! data password)
  (assert (bytevector? data))
  (assert (>= (bytevector-length data)
	      (max spake-public-data-bytes spake-response1-bytes)))
  (assert (string? password))

  (let ((pass-bv (string->utf8 password))
	(data-ptr (bytevector->pointer data))
	(client-state (make-spake-client-state)))
    (and (= 0 (&spake-step-1
	       (spake-client-state->pointer client-state)
	       data-ptr
	       data-ptr
	       (bytevector->pointer pass-bv)
	       (bytevector-length pass-bv)))
	 client-state)))

(define (spake-step-2! server-state data shadowed-password
		       server-id client-id)
  (assert (spake-server-state? server-state))
  (assert (bytevector? shadowed-password))
  (assert (= (bytevector-length shadowed-password) spake-stored-bytes))
  (assert (bytevector? data))
  (assert (>= (bytevector-length data)
	      (max spake-response1-bytes spake-response2-bytes)))
  (assert (bytevector? server-id))
  (assert (bytevector? client-id))

  (let* ((response-2 (make-bytevector spake-response2-bytes))
	 (ret
	  (= 0 (&spake-step-2
		(spake-server-state->pointer server-state)
		(bytevector->pointer response-2)
		(bytevector->pointer client-id)
		(bytevector-length client-id)
		(bytevector->pointer server-id)
		(bytevector-length server-id)
		(bytevector->pointer shadowed-password)
		(bytevector->pointer data)))))
    ;; NOTE: This is an unnecessary copy, but this implementation of
    ;;       spake doesn't allow the data buffer to be shared in this step,
    ;;       and I want the API to be consistent.
    (when ret (bytevector-copy! response-2 0 data 0 spake-response2-bytes))
    ret))

(define (spake-step-3! client-state data client-id server-id)
  (assert (spake-client-state? client-state))
  (assert (bytevector? data))
  (assert (>= (bytevector-length data)
	      (max spake-response2-bytes spake-response3-bytes)))
  (assert (bytevector? client-id))
  (assert (bytevector? server-id))

  (let ((data-ptr (bytevector->pointer data))
	(keys (make-spake-shared-keys)))
    (and (= 0 (&spake-step-3
	       (spake-client-state->pointer client-state)
	       data-ptr
	       (spake-shared-keys->pointer keys)
	       (bytevector->pointer client-id)
	       (bytevector-length client-id)
	       (bytevector->pointer server-id)
	       (bytevector-length server-id)
	       data-ptr))
	 (spake-shared-keys->sodium-encryption-keys-client keys))))

(define (spake-step-4! server-state data)
  (assert (spake-server-state? server-state))
  (assert (bytevector? data))
  (assert (>= (bytevector-length data) spake-response3-bytes))

  (let ((keys (make-spake-shared-keys)))
    (and (= 0 (&spake-step-4
	       (spake-server-state->pointer server-state)
	       (spake-shared-keys->pointer keys)
	       (bytevector->pointer data)))
	 (spake-shared-keys->sodium-encryption-keys-server keys))))
