;; Copyrignt (C) 2022 Jakub Wojciech
;;
;; This file is part of Larmo.
;;
;; Larmo is free software: you can redistribute it and/or modify it under
;; the terms of the GNU Affero General Public License as published by the
;; Free Software Foundation, either version 3 of the License, or (at your
;; option) any later version.
;;
;; Larmo is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
;; FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
;; License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with Larmo. If not, see <https://www.gnu.org/licenses/>.

(library (larmo crypto core)
  (export sodium-secretstream-aead-bytes
	  sodium-secretstream-header-bytes
	  sodium-secretstream-key-bytes
	  sodium-secretstream-tag-message
	  sodium-secretstream-tag-push
	  sodium-secretstream-tag-rekey
	  sodium-secretstream-tag-final
	  sodium-opslimit-min
	  sodium-opslimit-max
	  sodium-opslimit-interactive
	  sodium-opslimit-moderate
	  sodium-opslimit-sensitive
	  sodium-memlimit-min
	  sodium-memlimit-max
	  sodium-memlimit-interactive
	  sodium-memlimit-moderate
	  sodium-memlimit-sensitive
	  sodium-keygen
	  make-sodium-secretstream-header
	  sodium-secretstream-header?
	  sodium-secretstream-state?
	  sodium-secretstream-key?
	  sodium-secretstream-key-zero?
	  sodium-secretstream-init-push
	  sodium-secretstream-init-push!
	  sodium-secretstream-init-pull
	  sodium-secretstream-push!
	  sodium-secretstream-pull!)
  (import (guile)
	  (ice-9 control)
	  (srfi srfi-71)
	  (rnrs base)
	  (rnrs bytevectors)
	  (system foreign)
	  (system foreign-library)
	  (larmo util)))

(define libsodium (load-foreign-library "libsodium"))

(define sodium-secretstream-aead-bytes
  ((foreign-library-function
    libsodium
    "crypto_secretstream_xchacha20poly1305_abytes"
    #:return-type size_t)))

(define sodium-secretstream-header-bytes
  ((foreign-library-function
    libsodium
    "crypto_secretstream_xchacha20poly1305_headerbytes"
    #:return-type size_t)))

(define sodium-secretstream-key-bytes
  ((foreign-library-function
    libsodium
    "crypto_secretstream_xchacha20poly1305_keybytes"
    #:return-type size_t)))

(define sodium-secretstream-state-bytes
  ((foreign-library-function
    libsodium
    "crypto_secretstream_xchacha20poly1305_statebytes"
    #:return-type size_t)))

(define sodium-secretstream-tag-message
  ((foreign-library-function
    libsodium
    "crypto_secretstream_xchacha20poly1305_tag_message"
    #:return-type size_t)))

(define sodium-secretstream-tag-push
  ((foreign-library-function
    libsodium
    "crypto_secretstream_xchacha20poly1305_tag_push"
    #:return-type size_t)))

(define sodium-secretstream-tag-rekey
  ((foreign-library-function
    libsodium
    "crypto_secretstream_xchacha20poly1305_tag_rekey"
    #:return-type size_t)))

(define sodium-secretstream-tag-final
  ((foreign-library-function
    libsodium
    "crypto_secretstream_xchacha20poly1305_tag_final"
    #:return-type size_t)))

(define sodium-opslimit-min
  ((foreign-library-function libsodium "crypto_pwhash_opslimit_min"
			     #:return-type size_t)))
(define sodium-opslimit-max
  ((foreign-library-function libsodium "crypto_pwhash_opslimit_max"
			     #:return-type size_t)))
(define sodium-opslimit-interactive
  ((foreign-library-function libsodium "crypto_pwhash_opslimit_interactive"
			     #:return-type size_t)))
(define sodium-opslimit-moderate
  ((foreign-library-function libsodium "crypto_pwhash_opslimit_moderate"
			     #:return-type size_t)))
(define sodium-opslimit-sensitive
  ((foreign-library-function libsodium "crypto_pwhash_opslimit_sensitive"
			     #:return-type size_t)))
(define sodium-memlimit-min
  ((foreign-library-function libsodium "crypto_pwhash_memlimit_min"
			     #:return-type size_t)))
(define sodium-memlimit-max
  ((foreign-library-function libsodium "crypto_pwhash_memlimit_max"
			     #:return-type size_t)))
(define sodium-memlimit-interactive
  ((foreign-library-function libsodium "crypto_pwhash_memlimit_interactive"
			     #:return-type size_t)))
(define sodium-memlimit-moderate
  ((foreign-library-function libsodium "crypto_pwhash_memlimit_moderate"
			     #:return-type size_t)))
(define sodium-memlimit-sensitive
  ((foreign-library-function libsodium "crypto_pwhash_memlimit_sensitive"
			     #:return-type size_t)))


(define sodium-malloc
  (foreign-library-function libsodium "sodium_malloc"
			    #:return-type '*
			    #:arg-types `(,size_t)))
(define sodium-free
  (foreign-library-function libsodium "sodium_free"
			    #:arg-types '(*)))

(define sodium-mlock
  (foreign-library-function libsodium "sodium_mlock"
			    #:return-type int
			    #:arg-types `(* ,size_t)))

(define sodium-munlock
  (foreign-library-function libsodium "sodium_munlock"
			    #:return-type int
			    #:arg-types `(* ,size_t)))

(define sodium-memzero
  (foreign-library-function libsodium "sodium_memzero"
			    #:arg-types `(* ,size_t)))

(define (make-sodium-secretstream-state)
  ;; FIXME: Add automatic memory clearing
  (make-bytevector sodium-secretstream-state-bytes))

(define (sodium-secretstream-state? o)
  (and (bytevector? o)
       (= (bytevector-length o) sodium-secretstream-state-bytes)))

(define (sodium-secretstream-state->pointer o)
  (bytevector->pointer o))

(define (make-sodium-secretstream-key)
  (make-bytevector sodium-secretstream-key-bytes))

(define (sodium-secretstream-key? o)
  (and (bytevector? o)
       (= (bytevector-length o) sodium-secretstream-key-bytes)))

(define (sodium-secretstream-key-zero? o)
  (bytevector-empty? o))

(define (sodium-secretstream-key->pointer o)
  (bytevector->pointer o))

(define (make-sodium-secretstream-header)
  (make-bytevector sodium-secretstream-header-bytes))

(define (sodium-secretstream-header? o)
  (and (bytevector? o)
       (>= (bytevector-length o) sodium-secretstream-header-bytes)))

(define (sodium-secretstream-header->pointer o)
  (bytevector->pointer o))

(define sodium-keygen
  (let ((fun (foreign-library-function
	      libsodium
	      "crypto_secretstream_xchacha20poly1305_keygen"
	      #:arg-types '(*))))
    (lambda ()
      (let ((bv (make-sodium-secretstream-key)))
	(fun (sodium-secretstream-key->pointer bv))
	bv))))

(define &sodium-secretstream-init-push
  (foreign-library-function
   libsodium
   "crypto_secretstream_xchacha20poly1305_init_push"
   #:return-type int
   #:arg-types '(* * *)))

(define (sodium-secretstream-init-push key)
  (let* ((header (make-sodium-secretstream-header))
	 (state (sodium-secretstream-init-push! header key)))
    (and state (values state header))))

(define (sodium-secretstream-init-push! header key)
  (assert (sodium-secretstream-header? header))
  (assert (sodium-secretstream-key? key))

  (let ((state (make-sodium-secretstream-state)))
    (and (= 0 (&sodium-secretstream-init-push
	       (sodium-secretstream-state->pointer state)
	       (sodium-secretstream-header->pointer header)
	       (sodium-secretstream-key->pointer key)))
	 state)))

(define &sodium-secretstream-push
  (foreign-library-function
   libsodium
   "crypto_secretstream_xchacha20poly1305_push"
   #:return-type int
   #:arg-types `(* * * * ,size_t * ,size_t ,uint8)))

(define* (sodium-secretstream-push! state
				    ciphertext
				    cleartext cleartext-bytes
				    #:key
				    (additional-data %null-pointer)
				    (additional-data-length 0)
				    (tag sodium-secretstream-tag-message)
				    (ciphertext-start 0)
				    (cleartext-start 0))
  ;; FIXME: These assertions should exist only in debug mode
  (assert (sodium-secretstream-state? state))
  (assert (bytevector? ciphertext))
  (assert (bytevector? cleartext))
  (assert (or (bytevector? additional-data) (null-pointer? additional-data)))
  (assert (integer? cleartext-bytes))
  (assert (integer? additional-data-length))
  (assert (integer? tag))
  (assert (integer? ciphertext-start))
  (assert (integer? cleartext-start))

  (when (< (- (bytevector-length cleartext) cleartext-start)
	   cleartext-bytes)
    (error 'sodium-secretstream-push! "cleartext-bytes too big"
	   (list 'cleartext-length (bytevector-length cleartext))
	   (list 'cleartext-bytes cleartext-bytes)
	   (list 'cleartext-start cleartext-start)))

  (when (< (- (bytevector-length ciphertext) ciphertext-start)
	   (+ cleartext-bytes sodium-secretstream-aead-bytes))
    (error 'sodium-secretstream-push! "ciphertext buffer too small"
	   (list 'ciphertext-length ciphertext)
	   (list 'cleartext-bytes cleartext-bytes)))

  (define ciphertext-ptr
    (let ((bv-ptr (bytevector->pointer ciphertext)))
      (if (> ciphertext-start 0)
	(make-pointer (+ (pointer-address bv-ptr) ciphertext-start))
	bv-ptr)))

  (define cleartext-ptr
    (let ((bv-ptr (bytevector->pointer cleartext)))
      (if (> cleartext-start 0)
	(make-pointer (+ (pointer-address bv-ptr) cleartext-start))
	bv-ptr)))

  (= 0 (&sodium-secretstream-push
	(sodium-secretstream-state->pointer state)
	ciphertext-ptr %null-pointer
	cleartext-ptr cleartext-bytes
	(if (bytevector? additional-data)
	  (bytevector->pointer additional-data)
	  %null-pointer)
	additional-data-length
	tag)))


(define &sodium-secretstream-init-pull
  (foreign-library-function
   libsodium
   "crypto_secretstream_xchacha20poly1305_init_pull"
   #:return-type int
   #:arg-types '(* * *)))

(define (sodium-secretstream-init-pull header key)
  (assert (sodium-secretstream-header? header))
  (assert (sodium-secretstream-key? key))

  (let ((state (make-sodium-secretstream-state)))
    (and (= 0 (&sodium-secretstream-init-pull
	       (sodium-secretstream-state->pointer state)
	       (sodium-secretstream-header->pointer header)
	       (sodium-secretstream-key->pointer key)))
	 state)))

(define &sodium-secretstream-pull
  (foreign-library-function
   libsodium
   "crypto_secretstream_xchacha20poly1305_pull"
   #:return-type int
   #:arg-types `(* * * * * ,size_t * ,size_t)))

(define* (sodium-secretstream-pull! state
				    cleartext
				    ciphertext ciphertext-bytes
				    #:key
				    (additional-data %null-pointer)
				    (additional-data-length 0)
				    (cleartext-start 0)
				    (ciphertext-start 0))
  ;; FIXME: These assertions should exist only in debug mode
  (assert (sodium-secretstream-state? state))
  (assert (bytevector? ciphertext))
  (assert (bytevector? cleartext))
  (assert (or (bytevector? additional-data) (null-pointer? additional-data)))
  (assert (integer? ciphertext-bytes))
  (assert (integer? additional-data-length))
  (assert (integer? cleartext-start))
  (assert (integer? ciphertext-start))

  (when (< (- (bytevector-length ciphertext) ciphertext-start)
	   ciphertext-bytes)
    (error 'sodium-secretstream-pull! "ciphertext-bytes too big"
	   (list 'ciphertext-length (bytevector-length ciphertext))
	   (list 'ciphertext-bytes ciphertext-bytes)
	   (list 'ciphertext-start ciphertext-start)))

  (when (< (- (bytevector-length cleartext) cleartext-start)
	   (- ciphertext-bytes sodium-secretstream-aead-bytes))
    (error 'sodium-secretstream-pull! "cleartext buffer too small"
	   (list 'cleartext-length (bytevector-length cleartext))
	   (list 'ciphertext-bytes ciphertext-bytes)))

  (define ciphertext-ptr
    (let ((bv-ptr (bytevector->pointer ciphertext)))
      (if (> ciphertext-start 0)
	(make-pointer (+ (pointer-address bv-ptr) ciphertext-start))
	bv-ptr)))

  (define cleartext-ptr
    (let ((bv-ptr (bytevector->pointer cleartext)))
      (if (> cleartext-start 0)
	(make-pointer (+ (pointer-address bv-ptr) cleartext-start))
	bv-ptr)))

  (let ((tag (make-bytevector 1)))
    (values (= 0 (&sodium-secretstream-pull
		  (sodium-secretstream-state->pointer state)
		  cleartext-ptr %null-pointer
		  (bytevector->pointer tag)
		  ciphertext-ptr ciphertext-bytes
		  (if (bytevector? additional-data)
		      (bytevector->pointer additional-data)
		      additional-data)
		  additional-data-length))
	    (bytevector-u8-ref tag 0))))
