;; Copyrignt (C) 2022 Jakub Wojciech
;;
;; This file is part of Larmo.
;;
;; Larmo is free software: you can redistribute it and/or modify it under
;; the terms of the GNU Affero General Public License as published by the
;; Free Software Foundation, either version 3 of the License, or (at your
;; option) any later version.
;;
;; Larmo is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
;; FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
;; License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with Larmo. If not, see <https://www.gnu.org/licenses/>.

(library (larmo crypto encrypted-port)
  (export make-sodium-encryption-state
	  make-sodium-encryption-state!
	  make-sodium-encryption-state-complete!
	  sodium-encryption-state-finalize!
	  sodium-encryption-state?
	  sodium-encryption-keys?
	  make-encrypted-port)
  (import (guile)
	  (ice-9 control)
	  (rnrs base)
	  (rnrs bytevectors)
	  (rnrs io ports)
	  (srfi srfi-9)
	  (larmo crypto core)))

(define-record-type <encryption-keys>
  (&make-sodium-encryption-keys enc-key dec-key)
  sodium-encryption-keys?
  (enc-key sodium-encryption-keys-enc)
  (dec-key sodium-encryption-keys-dec))

(define-record-type <encryption-state>
  (&make-sodium-encryption-state enc-state dec-state)
  sodium-encryption-state?
  (enc-state sodium-encryption-state-enc)
  (dec-state sodium-encryption-state-dec))

(define (make-sodium-encryption-state! header keys)
  ;; TODO: Make HEADER accept a bare bytevector
  (assert (sodium-secretstream-header? header))
  (assert (sodium-encryption-keys? keys))
  (let ((enc-state (sodium-secretstream-init-push!
		    header
		    (sodium-encryption-keys-enc keys))))
    (lambda (peer-header)
      (&make-sodium-encryption-state
       enc-state
       (sodium-secretstream-init-pull peer-header
				      (sodium-encryption-keys-dec keys))))))

(define (make-sodium-encryption-state keys)
  (let ((header (make-sodium-secretstream-header)))
    (values (make-sodium-encryption-state! header keys)
	    header)))

(define (make-sodium-encryption-state-complete! header peer-header keys)
  "HEADER has to be a bytevector. The header needed to be sent to the peer
will be put there.
PEER-HEADER is the bytevector with a header received from peer.
HEADER and PEER-HEADER are allowed to be the same bytevector."
  (assert (sodium-secretstream-header? header))
  (assert (sodium-secretstream-header? peer-header))
  (assert (sodium-encryption-keys? keys))
  (let* ((dec-state (sodium-secretstream-init-pull
		     peer-header
		     (sodium-encryption-keys-dec keys)))
	 (enc-state (sodium-secretstream-init-push!
		     header
		     (sodium-encryption-keys-enc keys))))
    (&make-sodium-encryption-state enc-state dec-state)))

(define* (make-encrypted-port port handshake
			      #:optional (buffer-size-limit 4096))
  ;; TODO: Send some magic bytes before the handshake to check if the other
  ;;       side also tries to handshake.
  (define encryption-state (handshake port))
  (unless (sodium-encryption-state? encryption-state)
    (error 'make-encrypted-port "Handshake failed"))

  ;; TODO: Call this method on any error.
  (define (finalize)
    (pk "closing encrypted port" port)
    (close-port port))

  (define read!
    (let ((buffer (make-bytevector 128))
	  (size-bv (make-bytevector 4))
	  (index 0)
	  (left 0))
      (lambda (bv start count)
	;; (pk 'read)
	;; Return the number of bytes read.
	;; Read COUNT bytes from PORT, decrypt and write to BV at START.
	;; TODO: Find out what's the hot path and adjust the order.
	(let/ec return
	  (cond
	   ((zero? left)
	    ;; (pk '(zero? left))
	    ;; get the u32 msg length
	    ;; (pk 'pulling-size)
	    (let ((bytes (get-bytevector-n! port size-bv 0 4)))
	      (cond ((eof-object? bytes) (return 0))
		    ((< bytes 4) (throw 'message-too-short))))
	    (let ((ciphertext-bytes
		   (bytevector-u32-ref size-bv 0 (endianness little))))
	      ;; Extend the buffer if necessary.
	      (when (< (bytevector-length buffer) ciphertext-bytes)
		(when (> ciphertext-bytes buffer-size-limit)
		  (throw 'incoming-message-too-long))
		(set! buffer (make-bytevector ciphertext-bytes)))
	      ;; Read the whole ciphertext into buffer.
	      ;; (pk 'pulling-ciphertext)
	      (let ((bytes (get-bytevector-n!
			    port buffer 0 ciphertext-bytes)))
		(when (or (eof-object? bytes)
			  (< bytes ciphertext-bytes))
		  ;; EOF reached before reading the whole ciphertext.
		  (return 0)))
	      ;; NOTE: This loop is probably redundant due to how
	      ;;       get-bytevector-n! works. The above code should be
	      ;;       enough, but I want to test it first, so I won't
	      ;;       remove this yet.
	      ;; (let loop ((index 0)
	      ;; 		(to-read ciphertext-bytes))
	      ;;   (let ((read (get-bytevector-n! port buffer
	      ;; 				    index to-read)))
	      ;; 	 (cond ((zero? read) (return 0)
	      ;; 	       ((< read to-read)
	      ;; 		(loop (+ index read) (- to-read read)))))))
	      ;; (pk 'dec-state
	      ;; 	  (bytevector->hex
	      ;; 	   (sodium-encryption-state-dec encryption-state)))
	      (unless (sodium-secretstream-pull!
		       (sodium-encryption-state-dec encryption-state)
		       buffer buffer ciphertext-bytes
		       #:additional-data size-bv #:additional-data-length 4)
		;; FIXME: Proper error handling.
		(throw 'decryption-error))
	      (set! left (- ciphertext-bytes sodium-secretstream-aead-bytes))
	      (set! index 0)
	      (read! bv start count)))
	   (else
	    ;; (pk 'else)
	    (let ((bytes (if (> count left) left count)))
	      (bytevector-copy! buffer index bv start bytes)
	      (set! left (- left bytes))
	      (set! index (+ index bytes))
	      bytes)))))))

  (define write!
    (let ((buffer (make-bytevector 128))
	  (size-bv (make-bytevector 4)))
      (lambda (bv start count)
	;; (pk 'write)
	;; Return the number of bytes written.
	;; Read COUNT bytes from BV starting at START, encrypt them and
	;;       write to PORT.
	(cond
	 ((zero? count)
	  (finalize)
	  count)
	 (else
	  (let ((ciphertext-bytes (+ count sodium-secretstream-aead-bytes)))
	    (bytevector-u32-set! size-bv 0 ciphertext-bytes
				 (endianness little))
	    ;; Extend the buffer if needed
	    (when (> ciphertext-bytes (bytevector-length buffer))
	      (when (> ciphertext-bytes buffer-size-limit)
		(throw 'outgoing-message-too-long))
	      (set! buffer (make-bytevector ciphertext-bytes)))
	    ;; Encrypt
	    ;; (pk 'enc-state
	    ;; 	(bytevector->hex
	    ;; 	 (sodium-encryption-state-enc encryption-state)))
	    (unless (sodium-secretstream-push!
		     (sodium-encryption-state-enc encryption-state)
		     buffer bv count #:cleartext-start start
		     #:additional-data size-bv #:additional-data-length 4)
	      ;; FIXME: Proper error handling.
	      (throw 'encryption-error))
	    ;; Push to port
	    ;; (pk 'writing)
	    (put-bytevector port size-bv 0 4)
	    (put-bytevector port buffer 0 ciphertext-bytes)
	    (force-output port))))
	count)))

  (make-custom-binary-input/output-port
   "encrypted port" read! write! #f #f finalize))
