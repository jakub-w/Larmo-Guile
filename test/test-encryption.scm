#!/usr/bin/env sh
exec guile -s "$0" "$@"
!#

;; Copyrignt (C) 2022 Jakub Wojciech
;;
;; This file is part of Larmo.
;;
;; Larmo is free software: you can redistribute it and/or modify it under
;; the terms of the GNU Affero General Public License as published by the
;; Free Software Foundation, either version 3 of the License, or (at your
;; option) any later version.
;;
;; Larmo is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
;; FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
;; License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with Larmo. If not, see <https://www.gnu.org/licenses/>.

(eval-when (load compile)
  (add-to-load-path
   (canonicalize-path (string-append (dirname (current-filename)) "/.."))))

(use-modules (srfi srfi-64)
	     (srfi srfi-71)
	     (larmo crypto core)
	     (larmo crypto encrypted-port)
	     (larmo crypto encrypted-port spake)
	     (larmo crypto spake)
	     (larmo util)
	     (ice-9 control)
	     (ice-9 threads)
	     (rnrs bytevectors)
	     (rnrs io ports))

(if (not (test-runner-current))
    (test-runner-current (test-runner-create)))

(define cleartext (string->utf8 "My test message"))

(define-syntax-rule (with-exposed-private
			(module-path module-path* ...)
			(sym ...) body body* ...)
  (begin (define sym (@@ (module-path module-path* ...) sym)) ...
	 body body* ...))

(test-group
 "Sodium-Encryption-Core"

 (test-assert "sodium-keygen"
   (not (sodium-secretstream-key-zero? (sodium-keygen))))

 (define key (sodium-keygen))
 (define-values (encryption-state header)
   (sodium-secretstream-init-push key))

 (test-assert "sodium-secretstream-init-push"
   (sodium-secretstream-state? encryption-state))

 (test-assert "sodium-secretstream-init-push"
   (sodium-secretstream-header? header))

 (define decryption-state (sodium-secretstream-init-pull header key))
 (test-assert "sodium-secretstream-init-pull"
   (sodium-secretstream-state? decryption-state))

 (define ciphertext (make-bytevector
		     (+ (bytevector-length cleartext)
			sodium-secretstream-aead-bytes)))
 (test-assert "sodium-secretstream-push!"
   (sodium-secretstream-push! encryption-state
			      ciphertext
			      cleartext (bytevector-length cleartext)))

 ;; TODO: Tests for when the ciphertext buffer is too small, with variations
 ;;       of ciphertext-start.
 ;; TODO: Tests for cleartext-start.
 ;; TODO: Same for the pull function.

 (let* ((decrypted-ciphertext
	 (make-bytevector (bytevector-length cleartext)))
	(success tag (sodium-secretstream-pull!
		      decryption-state decrypted-ciphertext
		      ciphertext (bytevector-length ciphertext))))
   (test-assert "sodium-secretstream-pull!" success)
   (test-eq "sodium-secretstream-pull!"
     sodium-secretstream-tag-message
     tag)
   (test-equal "sodium-secretstream-pull!"
     cleartext decrypted-ciphertext))

 ;; TODO: Test decryption and encryption using the same buffer for both
 ;;       cleartext and ciphertext.

 )

(define (bytevector->hex bv)
  (apply string-append (map (lambda (n) (number->string n 16))
			    (bytevector->u8-list bv))))
;; (define (pks . args)
;;   (monitor (apply pk args)))

(define (dumb-handshake)
  (lambda (port)
    (define encryption-key (sodium-keygen))
    ;; (pks 'initiator-enc-key (bytevector->hex encryption-key))
    (put-bytevector port encryption-key)
    (force-output port)
    (define decryption-key
      (get-bytevector-n port sodium-secretstream-key-bytes))
    ;; (pks 'initiator-dec-key (bytevector->hex decryption-key))

    (define-values (state header)
      (make-sodium-encryption-state
       ((@@ (larmo crypto encrypted-port) &make-sodium-encryption-keys)
	encryption-key decryption-key)))
    (put-bytevector port header)
    (force-output port)
    (get-bytevector-n! port header 0 sodium-secretstream-header-bytes)
    (state header)))

(test-group
 "Sodium-Encryption"

 (define-values (peer1 peer2)
   (let ((sockets (socketpair PF_UNIX SOCK_STREAM 0)))
     (values (car sockets) (cdr sockets))))

 (define-values (peer1-encrypted peer2-encrypted)
   (parallel (make-encrypted-port peer1 (dumb-handshake))
	     (make-encrypted-port peer2 (dumb-handshake))))

 (test-assert "make-encrypted-port - no errors" peer1-encrypted)
 (test-assert "make-encrypted-port - no errors" peer2-encrypted)

 (put-bytevector peer1-encrypted cleartext) (force-output peer1-encrypted)
 (test-equal "encrypted-port encryption"
   cleartext
   (get-bytevector-n peer2-encrypted (bytevector-length cleartext)))

 (put-bytevector peer1-encrypted cleartext) (force-output peer1-encrypted)
 (test-equal "encrypted-port encryption - second round"
   cleartext
   (get-bytevector-n peer2-encrypted (bytevector-length cleartext)))

 (put-bytevector peer2-encrypted cleartext) (force-output peer2-encrypted)
 (test-equal "encrypted-port encryption - reverse direction"
   cleartext
   (get-bytevector-n peer1-encrypted (bytevector-length cleartext)))

 (put-bytevector peer2-encrypted cleartext) (force-output peer2-encrypted)
 (test-equal "encrypted-port encryption - reverse direction - second round"
   cleartext
   (get-bytevector-n peer1-encrypted (bytevector-length cleartext))))

(test-group
 "SPAKE2-EE"

 (define password "password")
 (define server-id (string->utf8 "SERVER"))
 (define client-id (string->utf8 "CLIENT"))

 (define shadowed-password (spake-shadow-password
			    password
			    #:ops-limit sodium-opslimit-min
			    #:mem-limit sodium-memlimit-min))
 (test-assert "spake-shadow-password" (bytevector? shadowed-password))
 (test-assert "spake-shadow-password"
   (not (bytevector-empty? shadowed-password)))

 (define data (make-bytevector spake-buffer-size 0))
 (define server-state (spake-step-0! data shadowed-password))
 (test-assert "spake-step-0!" (spake-server-state? server-state))
 (test-assert "spake-step-0!" (not (bytevector-empty? data)))

 (define previous-data (bytevector-copy data))
 (define client-state (spake-step-1! data password))
 (test-assert "spake-step-1!" (spake-client-state? client-state))
 (test-assert "spake-step-1!" (not (equal? data previous-data)))

 (bytevector-copy! data 0 previous-data 0 (bytevector-length data))
 (spake-step-2! server-state data shadowed-password server-id client-id)
 (test-assert "spake-step-2!" (not (equal? data previous-data)))

 (bytevector-copy! data 0 previous-data 0 (bytevector-length data))
 (define client-shared-keys
   (spake-step-3! client-state data client-id server-id))
 (test-assert "spake-step-3!" (sodium-encryption-keys? client-shared-keys))
 (test-assert "spake-step-3!" (not (equal? data previous-data)))

 (define server-shared-keys (spake-step-4! server-state data))
 (test-assert "spake-step-4!" (sodium-encryption-keys? server-shared-keys))


 (with-exposed-private (larmo crypto encrypted-port)
     (sodium-encryption-keys-enc
      sodium-encryption-keys-dec)
   (test-equal
       "spake - successful key exchange"
     (sodium-encryption-keys-enc client-shared-keys)
     (sodium-encryption-keys-dec server-shared-keys))

   (test-equal
       "spake - successful key exchange"
     (sodium-encryption-keys-dec client-shared-keys)
     (sodium-encryption-keys-enc server-shared-keys))))

;; TODO: Test the whole SPAKE for bad password

;; (test-skip)
(test-group
 "SPAKE2-EE handshake"

 (define password "password")
 (define server-id "SERVER")
 (define client-id "CLIENT")
 (define shadowed-password (spake-shadow-password
			    password
			    #:ops-limit sodium-opslimit-min
			    #:mem-limit sodium-memlimit-min))

 (define-values (peer1 peer2)
   (let ((sockets (socketpair PF_UNIX SOCK_STREAM 0)))
     (values (car sockets) (cdr sockets))))

 ;; FIXME: This is a bad design for testing this.
 ;;        It will not catch throws if they occur inside the thread. That
 ;;        would mean the second end of the handshake will hang, trying to
 ;;        read from the socket.
 ;;        NOTE: The error inside the thread will be completely silent.
 (define peer1-encrypted #f)
 (define t1
   (begin-thread
    (set! peer1-encrypted
      (make-encrypted-port
       peer1 (spake-handshake-client password client-id)))))

 (define peer2-encrypted
   (make-encrypted-port
    peer2 (spake-handshake-server shadowed-password server-id)))
 (join-thread t1)

 (test-assert "make-encrypted-port no errors" (port? peer1-encrypted))
 (test-assert "make-encrypted-port no errors"
   (not (port-closed? peer1-encrypted)))
 (test-assert "make-encrypted-port no errors" (port? peer2-encrypted))
 (test-assert "make-encrypted-port no errors"
   (not (port-closed? peer2-encrypted)))

 (put-bytevector peer1-encrypted cleartext) (force-output peer1-encrypted)
 (test-equal "encrypted-port encryption"
   cleartext
   (get-bytevector-n peer2-encrypted (bytevector-length cleartext)))

 (put-bytevector peer1-encrypted cleartext) (force-output peer1-encrypted)
 (test-equal "encrypted-port encryption - second round"
   cleartext
   (get-bytevector-n peer2-encrypted (bytevector-length cleartext)))

 (put-bytevector peer2-encrypted cleartext) (force-output peer2-encrypted)
 (test-equal "encrypted-port encryption - reverse direction"
   cleartext
   (get-bytevector-n peer1-encrypted (bytevector-length cleartext)))

 (put-bytevector peer2-encrypted cleartext) (force-output peer2-encrypted)
 (test-equal "encrypted-port encryption - reverse direction - second round"
   cleartext
   (get-bytevector-n peer1-encrypted (bytevector-length cleartext))))



(let ((runner (test-runner-current)))
  (exit (if (zero? (+ (test-runner-fail-count runner)
		      ;; (test-runner-xpass-count runner)
		      (test-runner-xfail-count runner)))
	    EXIT_SUCCESS
	    EXIT_FAILURE)))

;; Local Variables:
;; eval: (put 'with-exposed-private 'scheme-indent-function 2)
;; mode: scheme
;; End:
